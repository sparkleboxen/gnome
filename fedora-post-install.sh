#!/bin/bash

# https://github.com/David-Else/fedora-ultimate-setup-script/blob/master/fedora-ultimate-setup-script.sh
# SHOULD THIS BE RUN AS SUDO???
# HOW TO ENSURE PROPER OWNERSHIP OF FILES?
# DO DIRS NEED TRAILING /?

GREEN=$(tput setaf 2)
BOLD=$(tput bold)
RESET=$(tput sgr0)

if [ "$(id -u)" = 0 ]; then
    echo "You're root! Run script as user" && exit 1
fi

# >>>>>> start of user settings <<<<<<

#==============================================================================
# gnome desktop settings
#==============================================================================
enable_animations="false" # enable animations
file_delete="true" # show files delete option
file_history="false" # disable file history
font_bold="Noto Sans Bold 9"
font_monospace="Terminus 9"
font_primary="Noto Sans 9"
gtk_theme="Materia-compact" # set gtk theme
hide_user_list="true" # disable gdm show accounts
icon_theme="Suru++"
idle_delay=900 # set screen time-out
natural_scroll="false" # disable natural-scroll
night_light="true" # enable night light
shell_theme="Materia-dark-compact" # set gnome-shell theme
sort_folders="true" # sort folders before files
thumbnail_limit=4096 # set file size limit for thumbnails
wallpaper="$HOME/.config/wallpaper/fedora-wallpaper.jpeg" # set wallpaper

#==============================================================================
# git settings
#==============================================================================
git_email='challenge@sparkleboxen.com'
git_user_name='sparkleboxen'

# >>>>>> end of user settings <<<<<<

#==============================================================================
# display user settings
#==============================================================================
clear
cat <<EOL
Do not run this script more than once or you may get duplication in settings files

${BOLD}Gnome settings${RESET}
${BOLD}-------------------${RESET}

Enable desktop animations: ${GREEN}$enable_animations${RESET}
Enable natural scrolling for mouse and trackpad: ${GREEN}$natural_scroll${RESET}
Show "delete file" option in Gnome Files: ${GREEN}$file_delete${RESET}
Set file size limit for thumbnails: ${GREEN}$thumbnail_limit${RESET} MB
Enable file history: ${GREEN}$file_history${RESET}
Sort folders before files: ${GREEN}$sort_folders${RESET}
Set primary font: ${GREEN}$font_primary${RESET}
Set bold font: ${GREEN}$font_bold${RESET}
Set monospace font: ${GREEN}$font_monospace${RESET}
Set gtk theme to: ${GREEN}$gtk_theme${RESET}
Set icon theme to: ${GREEN}$icon_theme${RESET}
Set gnome-shell theme to: ${GREEN}$shell_theme${RESET}
Increase the delay before the desktop logs out: ${GREEN}$idle_delay${RESET} seconds
Turn on night light: ${GREEN}$night_light${RESET}

${BOLD}Git settings${RESET}
${BOLD}-------------------${RESET}

Global email: ${GREEN}$git_email${RESET}
Global user name: ${GREEN}$git_user_name${RESET}

EOL

read -rp "Press enter to setup, or ctrl+c to quit"

#==============================================================================
# set host name
#==============================================================================
read -rp "What is this computer's name? [$HOSTNAME] " hostname
if [[ ! -z "$hostname" ]]; then
    hostnamectl set-hostname "$hostname"
fi

#
# VPN (DO ON ROUTER)
#
# wget -qP /tmp/ https://cdn.ivpn.net/releases/linux/2.12.5/ivpn-2.12.5-1.x86_64.rpm
#cp ivpn*.service /etc/systemd/system # complete command?
#systemctl daemon-reload
#systemctl start ivpn-service.service
#systemctl start ivpn-autoconnect.service
#systemctl enable ivpn-service.service
#systemctl enable ivpn-autoconnect.service
#ivpn login # complete command?

#==============================================================================
# Files & Directories
#==============================================================================

mkdir -p $HOME/{aud,bin,cld,doc,dwn,gam,img,mnt/pinky,mnt/sue,.ssh,vid}
mv $HOME/{Public,Desktop,Templates} $HOME/.local/share/
rm -R $HOME/{Downloads,Music,Documents,Pictures,Videos}

cat <<EOF > $HOME/.config/user-dirs.dirs
# This file is written by xdg-user-dirs-update
# If you want to change or add directories, just edit the line you're
# interested in. All local changes will be retained on the next run.
# Format is XDG_xxx_DIR="$HOME/yyy", where yyy is a shell-escaped
# homedir-relative path, or XDG_xxx_DIR="/yyy", where /yyy is an
# absolute path. No other format is supported.
 
XDG_DESKTOP_DIR="$HOME/.local/share/Desktop"
XDG_DOWNLOAD_DIR="$HOME/dwn"
XDG_TEMPLATES_DIR="$HOME/.local/share/"
XDG_PUBLICSHARE_DIR="$HOME/.local/share/"
XDG_DOCUMENTS_DIR="$HOME/doc"
XDG_MUSIC_DIR="$HOME/aud"
XDG_PICTURES_DIR="$HOME/img"
XDG_VIDEOS_DIR="$HOME/vid"
EOF

chmod 700 $HOME/.ssh/
chmod 600 $HOME/.ssh/*

#==============================================================================
# Install Repositories
#==============================================================================

wget -qP /tmp/ https://github.com/rpmsphere/noarch/tree/master/r/rpmsphere-release*rpm
rpm -Uvh /tmp/rpmsphere-release*rpm

sudo dnf -y install http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/{free/fedora/rpmfusion-free,nonfree/fedora/rpmfusion-nonfree}-release-$(rpm -E %fedora).noarch.rpm
sudo dnf config-manager --add-repo https://download.opensuse.org/repositories/home:TheLocehiliosan:yadm/Fedora_32/home:TheLocehiliosan:yadm.repo
sudo dnf copr enable provessor/golang-github-gokcehan-lf
sudo flatpak -y remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak -y remote-add --if-not-exists plex-media-player https://flatpak.knapsu.eu/plex-media-player.flatpakrepo

#==============================================================================
# Install Packages
#==============================================================================

sudo dnf -y update

sudo dnf -y install \
bleachbit \
calibre \
compat-ffmpeg28 \
darktable \
deluge-daemon \
deluge-web \
ffmpeg \
ffmpegthumbnailer \
firefox-wayland \
fuse-sshfs \
gnome-epub-thumbnailer \
gnome-tweaks \
google-noto-sans-fonts \
google-noto-serif-fonts \
gstreamer1-libav \
gstreamer1-plugins-ugly \
gthumb \
gtkhash \
gtkhash-nautilus \
hardinfo \
libwebp-tools \
lf \
lollypop \
lynis \
materia-gtk-theme \
mpv \
nextcloud-client \
raw-thumbnailer \
syncthing \
terminus-fonts \
tldr \
tmux \
unrar \
vim \
yadm \
youtube-dl \
zsh \
zsh-syntax-highlighting

#==============================================================================
# Install Flatpaks
#==============================================================================

sudo flatpak install flathub com.bitwarden.desktop \
org.gimp.GIMP \
com.sublimetext.three

#==============================================================================
# Fonts
#==============================================================================

gsettings set org.gnome.desktop.wm.preferences titlebar-font $font_bold
gsettings set org.gnome.desktop.interface font-name $font_primary
gsettings set org.gnome.desktop.interface document-font-name $font_primary
gsettings set org.gnome.desktop.interface monospace-font-name $font_monospace

sudo cat <<EOF > /etc/X11/Xresources
! This is the global resources file that is loaded when
! all users log in, as well as for the login screen

! Fix the Xft dpi to 96; this prevents tiny fonts
! or HUGE fonts depending on the screen size.
Xft.dpi: 96

! hintstyle: medium means that (for Postscript fonts) we
! position the stems for maximum constrast and consistency
! but do not force the stems to integral widths. hintnone,
! hintslight, and hintfull are the other possibilities.
Xft.hinting: true
Xft.autohint: false
Xft.rgba: rgb
Xft.lcdfilter: lcddefault
Xft.hintstyle: hintslight
EOF

sudo cat <<EOF > /etc/fonts/local.conf
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
 <match target="font" >
     <edit mode="assign" name="autohint" >
         <bool>false</bool>
     </edit>
 </match>
 <match target="font" >
     <edit mode="assign" name="rgba" >
         <const>rgb</const>
     </edit>
 </match>
 <match target="font" >
     <edit mode="assign" name="hinting" >
         <bool>true</bool>
     </edit>
 </match>
 <match target="font" >
     <edit mode="assign" name="hintstyle" >
         <const>hintslight</const>
     </edit>
 </match>
 <match target="font" >
     <edit mode="assign" name="antialias" >
         <bool>true</bool>
     </edit>
 </match>
 <match target="font" >
     <edit mode="assign" name="lcdfilter" >
         <const>lcddefault</const>
     </edit>
 </match>
</fontconfig>
EOF

sudo ln -s /usr/share/fontconfig/conf.avail/11-lcdfilter-default.conf /etc/fonts/conf.d/

#==============================================================================
# Icons
#==============================================================================

wget -qP /tmp/ -O suru++_install "https://raw.githubusercontent.com/gusbemacbe/suru-plus/master/install.sh"
chmod +x /tmp/suru++_install
/tmp/suru++_install

gsettings set org.gnome.desktop.interface.icon-theme $icon_theme

#==============================================================================
# Tweaks
#==============================================================================

#gsettings/config dock icon order
echo "${BOLD}Setting up Gnome desktop gsettings...${RESET}"

# dconf watch /
# https://medium.com/@ankurloriya/install-gnome-extension-using-command-line-736199be1cda

# Possible extensions:
# drop-down-terminal
# topicons-plus

# THESE NEED TO BE DOWNLOADED BEFORE INSTALLING???
gnome-extensions install dash-to-dock@micxgx.gmail.com \
disable-workspace-switcher-popup@github.com \
hide-activities-button@gnome-shell-extensions.bookmarkd.xyz \
KeepAwake@jepfa.de \
user-theme@gnome-shell-extensions.gcampax.github.com

gnome-extensions enable dash-to-dock@micxgx.gmail.com \
disable-workspace-switcher-popup@github.com \
hide-activities-button@gnome-shell-extensions.bookmarkd.xyz \
KeepAwake@jepfa.de \
user-theme@gnome-shell-extensions.gcampax.github.com

#SOME OF THESE DON'T SEEM TO BE FORMATED CORRECTLY (dash-to-dock)
gsettings set org.gnome.desktop.background picture-uri $wallpaper
gsettings set org.gnome.desktop.interface enable-animations $enable_animations
gsettings set org.gnome.desktop.interface.gtk-theme $gtk_theme
gsettings set org.gnome.desktop.peripherals.mouse natural-scroll $natural_scroll
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll $natural_scroll
gsettings set org.gnome.desktop.privacy.remember-recent-files $file_history
gsettings set org.gnome.desktop.session idle-delay $idle_delay
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-1 '["<Shift><Super>exclam"]'
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-2 '["<Shift><Super>at"]'
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-3 '["<Shift><Super>numbersign"]'
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-4 '["<Shift><Super>dollar"]'
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-last '["<Shift><Super>percent"]'
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 '["<Super>1"]'
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 '["<Super>2"]'
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 '["<Super>3"]'
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 '["<Super>4"]'
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-last '["<Super>5"]'
gsettings set org.gnome.login-screen.disable-user-list $hide_user_list
gsettings set org.gnome.nautilus.preferences thumbnail-limit $thumbnail_limit
gsettings set org.gnome.nautilus.preferences.show-delete-permanently $file_delete
gsettings set org.gnome.settings-daemon.plugins.color.night-light-enabled $night_light
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "GNOME Terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "gnome-terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Super>Return"
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"
gsettings set org.gnome.shell.extensions.dash-to-dock apply-custom-theme "true"
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size "24"
gsettings set org.gnome.shell.extensions.dash-to-dock hot-keys "false"
gsettings set org.gnome.shell.extensions.dash-to-dock show-mounts "false"
gsettings set org.gnome.shell.extensions.dash-to-dock show-trash "false"
gsettings set org.gnome.shell.extensions.user-theme name $shell_theme
gsettings set org.gnome.shell.keybindings switch-to-application-1 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-2 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-3 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-4 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-5 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-6 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-7 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-8 "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-9 "[]"
gsettings set org.gtk.settings.file-chooser.sort-directories-first $sort_folders

#==============================================================================
# Colors & Themes
#==============================================================================

$HOME/bin/git/gnome-terminal/install.sh # gnome-terminal dracula; back up directory to git - DO THIS IN SCRIPT???

#==============================================================================
# Hardinfo
#==============================================================================

wget -qP /tmp/ https://github.com/rpmsphere/noarch/tree/master/r/rpmsphere-release*rpm
rpm -Uvh /tmp/rpmsphere-release*rpm
dnf install hardinfo

#==============================================================================
# SUBLIME
#==============================================================================
# add *.sublime-license files to *.gitignore

#==============================================================================
# Firefox
#==============================================================================

# this doesn't work:
firefox -CreateProfile "DefaultProfile /home/fedora/.mozilla/firefox/DefaultProfile"
#mv $HOME/.mozilla/firefox/user.js $HOME/.mozilla/firefox/DefaultProfile/

#==============================================================================
# Plex
#==============================================================================

sudo flatpak -y install plex-media-player tv.plex.PlexMediaPlayer

#==============================================================================
# Config Files
#==============================================================================

#yadm decrypt

#==============================================================================
# Misc.
#==============================================================================
sudo chsh melonbear # /usr/bin/zsh
rm $HOME/.bash*
