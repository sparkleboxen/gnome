#!/bin/bash

#
# RUN AS SUDO!!!
#

#==============================================================================
# General Steps
#==============================================================================

# Set SELinux to permissive
setenforce Permissive

# Mono repo
rpm --import "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF"
su -c 'curl https://download.mono-project.com/repo/centos8-stable.repo | tee /etc/yum.repos.d/mono-centos8-stable.repo'
dnf update

# Install Packages
dnf -y install curl \
easytag \
gettext \
libmediainfo \
libzen \
mediainfo \
mono-core \
mono-devel \
mono-locale-extras \
sqlite.x86_64

# Mono
ln -s /opt/mono/bin/mono /usr/bin/mono

#==============================================================================
# lidarr
#==============================================================================
wget https://github.com/lidarr/Lidarr/releases/download/v0.6.2.883/Lidarr.develop.0.6.2.883.linux.tar.gz
tar -xzvf Lidarr.*.linux.tar.gz
chown -R melonbear:melonbear   /opt/Lidarr
chmod -R a=,a+X,u+rw,g+r /opt/Lidarr

cat <<EOF > /etc/systemd/system/lidarr.service
[Unit]
Description=Lidarr Daemon
After=network.target

[Service]
User=melonbear
Group=melonbear
Type=simple
ExecStart=/usr/bin/mono /opt/Lidarr/Lidarr.exe -nobrowser
TimeoutStopSec=20
KillMode=process
Restart=on-failure
[Install]
WantedBy=multi-user.target

EOF

systemctl enable lidarr.service
systemctl start lidarr.service

#==============================================================================
# lutris
#==============================================================================
dnf -y install lutris

#==============================================================================
# plex
#==============================================================================

cat <<EOF > /etc/yum.repos.d/plex.repo
[PlexRepo]
name=PlexRepo
baseurl=https://downloads.plex.tv/repo/rpm/$basearch/
enabled=1
gpgkey=https://downloads.plex.tv/plex-keys/PlexSign.key
gpgcheck=1
EOF

dnf -y install plexmediaserver

#==============================================================================
# radarr
#==============================================================================
curl -L -O $( curl -s https://api.github.com/repos/Radarr/Radarr/releases | grep linux.tar.gz | grep browser_download_url | head -1 | cut -d \" -f 4 )
tar -xvzf Radarr.develop.*.linux.tar.gz
mv Radarr /opt

cat <<EOF > /etc/systemd/system/radarr.service
[Unit]
Description=Radarr Daemon
After=syslog.target network.target

[Service]
# Change the user and group variables here.
User=melonbear
Group=melonbear

Type=simple

# Change the path to Radarr or mono here if it is in a different location for you.
ExecStart=/usr/bin/mono --debug /opt/Radarr/Radarr.exe -nobrowser -data=/home/melonbear/.config/Radarr
TimeoutStopSec=20
KillMode=process
Restart=on-failure

# These lines optionally isolate (sandbox) Radarr from the rest of the system.
# Make sure to add any paths it might use to the list below (space-separated).
#ReadWritePaths=/opt/Radarr /path/to/movies/folder
#ProtectSystem=strict
#PrivateDevices=true
#ProtectHome=true

[Install]
WantedBy=multi-user.target
EOF

systemctl enable radarr.service
systemctl start radarr.service

#==============================================================================
# sonarr
#==============================================================================
wget http://download.sonarr.tv/v2/master/mono/NzbDrone.master.tar.gz
tar -xvf ~/NzbDrone.master.tar.gz -C /opt/

chown -R melonbear:melonbear /opt/NzbDrone # change to merlin

cat <<EOF > /etc/systemd/system/sonarr.service
[Unit]
Description=Sonarr Daemon
After=syslog.target network.target

[Service]
User=melonbear
Group=melonbear
Type=simple
ExecStart=/usr/bin/mono /opt/sonarr/bin/NzbDrone.exe -nobrowser -data /home/melonbear/.config/sonarr
TimeoutStopSec=20

[Install]
WantedBy=multi-user.target
EOF

systemctl enable sonarr.service
systemctl start sonarr.service

# Firewall
cat > sonarr.xml << EOF
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>sonarr</short>
  <description>Sonarr Download Service</description>
  <port protocol="tcp" port="8989"/>
</service>
EOF
mv sonarr.xml /etc/firewalld/services/
firewall-cmd --permanent --add-service sonarr
firewall-cmd --reload

#==============================================================================
# Jackett
#==============================================================================

#==============================================================================
# Steam
#==============================================================================
dnf install steam

#==============================================================================
# UPS Software
#==============================================================================